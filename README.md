**Hop!**
A simple Spigot plugin that allows players to save locations privately, list those locations, and return to them.