package com.sg.mc.hop;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class HopListCommand extends BaseCommand implements CommandExecutor {


	public HopListCommand(JavaPlugin plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
		if(!checkSender(commandSender)) {
			return false;
		}
		Player p = (Player)commandSender;
		@SuppressWarnings("unchecked")
		List<HopLocation> locs = (List<HopLocation>)plugin.getConfig().getList("locs." + p.getUniqueId(), new ArrayList<>());
		locs.forEach(loc -> p.sendMessage(loc.getName() + ": "
							+ String.format("%.2f", loc.getLocation().getX())
							+ ", "
							+ String.format("%.2f", loc.getLocation().getY())
							+ ", "
							+ String.format("%.2f", loc.getLocation().getZ())));
		return true;
	}

}
