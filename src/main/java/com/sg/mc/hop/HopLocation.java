package com.sg.mc.hop;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

public class HopLocation implements ConfigurationSerializable {
	private String name;
	private Location location;


	public HopLocation() {
		super();
	}

	public HopLocation(String name, Location location) {
		super();
		this.name = name;
		this.location = location;
	}

	public HopLocation(Map<String, Object> fields) {
		this.name = (String)fields.get("name");
		this.location = (Location)fields.get("location");
	}
	
	public static final HopLocation make(String name, Location location) {
		return new HopLocation(name, location);
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> result = new HashMap<>();
		result.put("name", name);
		result.put("location", location);
		return result;
	}
}
