package com.sg.mc.hop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class HopSaveCommand extends BaseCommand implements CommandExecutor {


	public HopSaveCommand(JavaPlugin plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
		if(!checkSender(commandSender)) {
			return false;
		}
		Player p = (Player)commandSender;
		if(args.length == 0) {
			p.sendMessage("You must include a name for this hop");
			return false;
		}
		
		final String hopName = Arrays.stream(args).collect(Collectors.joining(" ")).trim();
		
		@SuppressWarnings("unchecked")
		List<HopLocation> locs = (List<HopLocation>)plugin.getConfig().getList("locs." + p.getUniqueId().toString(), new ArrayList<>());
		HopLocation loc = locs.stream().filter(l -> hopName.equalsIgnoreCase(l.getName())).findAny().orElse(null);
		if(loc != null) {
			p.sendMessage("A hop with the name \"" + hopName + "\" already exists.");
			return true;
		}
		
		loc = HopLocation.make(hopName, p.getLocation());
		locs.add(loc);
		plugin.getConfig().set("locs." + p.getUniqueId().toString(), locs);
		plugin.saveConfig();
		p.sendMessage("Hop saved! Use \"/hop " + hopName + "\" to return here!");
		return true;
	}

}
