package com.sg.mc.hop;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class HopLocCommand extends BaseCommand implements CommandExecutor {

	private static final String SCOLD = "You must specify exactly three numbers, such as /hoploc 20.5 62 97.3";
	public HopLocCommand(JavaPlugin plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
		
		if(!checkSender(commandSender)) {
			return false;
		}
		Player p = (Player)commandSender;
		if(args.length != 3) {
			p.sendMessage(SCOLD);
			return false;
		}
		
		
		try {
			double x = Double.valueOf(args[0]);
			double y = Double.valueOf(args[1]);
			double z = Double.valueOf(args[2]);
			p.teleport(new Location(p.getWorld(), x, y, z));
		} catch (NumberFormatException nfe) {
			p.sendMessage(SCOLD);
			return false;
		}
		
		return true;
	}

}
