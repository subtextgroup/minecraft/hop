package com.sg.mc.hop;

import java.io.File;

import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

public class HopControl extends JavaPlugin {

	
	
	@Override
	public void onEnable() {
		ConfigurationSerialization.registerClass(HopLocation.class);
		@SuppressWarnings("unused")
		Configuration config = createConfig();
		
		
		
		this.getCommand("hop").setExecutor(new HopCommand(this));
		this.getCommand("hoploc").setExecutor(new HopLocCommand(this));
		this.getCommand("hoplist").setExecutor(new HopListCommand(this));
		this.getCommand("hopsave").setExecutor(new HopSaveCommand(this));
		this.getCommand("hopdelete").setExecutor(new HopDeleteCommand(this));
		
		getServer().broadcastMessage("Hop plugin enabled! Check out /hop, /hopsave, /hoploc, /hopdelete, /hoplist!");
		
		super.onEnable();
	}


	private Configuration createConfig() {
	    try {
	        if (!getDataFolder().exists()) {
	            getDataFolder().mkdirs();
	        }
	        File file = new File(getDataFolder(), "config.yml");
	        if (!file.exists()) {
	            getLogger().info("Config.yml not found, creating!");
	            saveDefaultConfig();
	        } else {
	            getLogger().info("Config.yml found, loading!");
	        }
	    } catch (Exception e) {
	        e.printStackTrace();

	    }
	    return getConfig();
	}
}
