package com.sg.mc.hop;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class BaseCommand {
	
	protected JavaPlugin plugin;
	public BaseCommand(JavaPlugin plugin) {
		this.plugin = plugin;
	}
	
	protected boolean checkSender(CommandSender sender) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("Only available to players. You are not a player.");
			return false;
		} else {
			return true;
		}
	}
}
